import DataBinder from "../DataBinder";
import css from "./index.css";

let time = {
  a: { b: { second: "" } }
};

const viewTemplate = `<p class="secondsCounter" data-bind="a.b.second"></p>`;
const dataBinder = new DataBinder();
const view = dataBinder.bind(time, viewTemplate);
const app = document.getElementById("app");
app.appendChild(view);

let seconds = 1;
const timerId = setInterval(() => {
  time.a.b.second = `You are ${seconds}s on this page`;
  seconds += 1;
}, 1000);
