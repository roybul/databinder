import DataBinder from "../DataBinder";
import { imagesPaths } from "./constants";
import addArrows from "./arrows";
import css from "./index.css";

const images = {
  path: "img/1s.jpg",
  alt: "some image"
};

const myFunction = () => {
  console.log("x");
};
const viewTemplate = `<img id="bindedImage" class="bindedImage" data-bind-src="path" data-bind-alt="alt" />`;

const dataBinder = new DataBinder();
const view = dataBinder.bind(images, viewTemplate);
const domElement = document.getElementById("images");
addArrows(images);
domElement.appendChild(view);

const bindedImage = document.getElementById("bindedImage");
const handleImageClick = () => {
  const currentPath = images.path;
  const isImageSmall = currentPath.includes("s");
  const currentIndex = parseInt(currentPath.replace(/\D/g, ""));

  images.path = isImageSmall
    ? `img/${currentIndex}.jpg`
    : `img/${currentIndex}s.jpg`;
};

bindedImage.addEventListener("click", handleImageClick);
