export default images => {
  const domElement = document.getElementById("images");
  domElement.innerHTML = `<div id="left" class="left"></div> <div id="right" class="right"/></div>`;

  const getImageData = () => {
    const currentPath = images.path;
    const currentIndex = parseInt(currentPath.replace(/\D/g, ""));
    const isImageSmall = currentPath.includes("s");
    const imageSize = isImageSmall ? "s" : "";
    return { currentIndex, imageSize };
  };

  const handleLeftClick = () => {
    const { imageSize, currentIndex } = getImageData();
    images.path =
      currentIndex === 1
        ? `img/16${imageSize}.jpg`
        : `img/${currentIndex - 1 + imageSize}.jpg`;
  };
  const handleRightClick = () => {
    const { imageSize, currentIndex } = getImageData();
    images.path =
      currentIndex === 16
        ? `img/1${imageSize}.jpg`
        : `img/${currentIndex + 1 + imageSize}.jpg`;
  };

  const left = document.getElementById("left");
  const right = document.getElementById("right");
  left.addEventListener("click", handleLeftClick);
  right.addEventListener("click", handleRightClick);
};
