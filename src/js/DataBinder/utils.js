export const parseStringToHtmlElement = template =>
  new DOMParser().parseFromString(template, "text/html").body.firstChild;

export const pickValueFromModel = (path, model) => {
  const pathArr = path.split(".");
  try {
    const value = pathArr.reduce((acc, item) => {
      return acc[item];
    }, model);
    return value;
  } catch (err) {
    return null;
  }
};

export const createSetter = (bindingModel, datasetBindEntry, view) => value => {
  const [attributeToSet, modelPath] = datasetBindEntry;
  if (!attributeToSet) {
    view.innerHTML = value;
  } else {
    view.setAttribute(attributeToSet, value);
  }
  bindingModel[modelPath] = value;
};

export const createGetter = (bindingModel, datasetBindEntry) => () => {
  const [_, modelPath] = datasetBindEntry;
  return bindingModel[modelPath];
};
