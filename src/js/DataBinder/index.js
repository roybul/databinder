import { PERMITTED_ATTRIBUTES } from "./constants";
import {
  parseStringToHtmlElement,
  pickValueFromModel,
  createGetter,
  createSetter
} from "./utils";

export default class DataBinder {
  bindingModel = {};

  datasetBindEntries = [];

  bind = (model, template) => {
    const parsedTemplate = parseStringToHtmlElement(template);
    this.getDatasetBindEntries(parsedTemplate);
    this.bindAttributesToView(parsedTemplate, model);
    this.addObserversToModel(model, parsedTemplate);
    return parsedTemplate;
  };

  addObserversToModel = (model, view) => {
    this.datasetBindEntries.forEach(bindEntry => {
      let path = bindEntry[1];
      let nestedObj;

      if (path.includes(".")) {
        const nestedPathArr = path.split(".");
        path = nestedPathArr.pop();
        nestedObj = pickValueFromModel(nestedPathArr.join("."), model);
      }

      const valueToBind = pickValueFromModel(path, model);
      this.bindingModel[path] = valueToBind;

      const set = createSetter(this.bindingModel, bindEntry, view);
      const get = createGetter(this.bindingModel, bindEntry);

      Object.defineProperty(nestedObj ? nestedObj : model, path, { set, get });
    });
  };

  getDatasetBindEntries = view =>
    Object.entries(view.dataset).forEach(entryPair => {
      const [attr, path] = entryPair;
      if (attr.includes("bind")) {
        const attributeName = attr.replace("bind", "").toLowerCase();

        this.datasetBindEntries.push([attributeName, path]);
      }
    });

  bindAttributesToView = (view, model) => {
    const datasetAttributes = view.dataset;

    this.datasetBindEntries.forEach(entryPair => {
      const [attributeName, modelPath] = entryPair;
      const valueToBind = pickValueFromModel(modelPath, model);

      if (!attributeName) {
        view.innerHTML = valueToBind;
      } else {
        // pickValueFromModel return null if model property does not exist, but there is a binding declaration in view
        valueToBind !== null &&
          // cheking for permitted attributes ("href", "src", "alt", "title")
          PERMITTED_ATTRIBUTES.includes(attributeName) &&
          view.setAttribute(attributeName, valueToBind);
      }
    });
  };
}
